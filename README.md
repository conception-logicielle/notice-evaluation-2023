# Notice pour l'évaluation du cours de conception logicielle 2023

## Modalités

### Contexte

Vous allez devoir réaliser un projet, soit en prenant 1 sujet parmi ceux proposés, soit un projet que vous proposez de concevoir, a condition qu'il comporte une utilité pédagogique validée, la date limite de proposition des projets est fixée au 20/03/2023 a 13h30.

Le projet choisi sera réalisable à jusqu'à 4 personnes. L'évaluation sera adaptée en conséquence .

Pour cela une feuille sheets est disponible pour renseigner le sujet par elève ici  : [TODO]. 
### Calendrier

La notice d'évaluation est donnée le 13 mars 2023.
Le rendu final est exigé pour le vendredi 24 mars à 22h.  

### Rendu attendu

Le rendu attendu est un dépôt git **public**, hébergé sur **votre compte personnel** ou **un compte de groupe/organization** sur la plateforme de votre choix (par défaut, gitlab.com mais vous êtes libres de choisir une plateforme concurrente tel que github ou bitbucket).  
Le lien de votre dépôt git devra être envoyé par mail à `antoine.brunetti@insee.fr` avant la deadline de vendredi 24 mars à 22h.

### Critères d'évaluation

Les critères d'évaluation sont les suivants :

- Un dépôt git bien entretenu
  - Le dépôt est public
  - Les fichiers versionnés respectent les bonnes pratiques (texte brut, uniquement les sources pas les produits ...)
  - L'historique permet de retracer les différentes étapes du développement (pas un simple unique commit)
  - Bonus : utilisation opportune des branches, issues et merge requests
- Du code industriel
  - Les dépendances sont clairement listées et installables en une commande
  - Le projet est lançable avec le minimum d'opérations manuelles
- Des applicatifs fonctionnels:
  - Une application qui démarre
  - Un scénario d'utilisation de l'application (soit dans la doc/ soit en tant que script)
  - Bonus: De la gestion de cas limites, documentée dans le projet via commentaires.
- De la documentation
  - L'objectif, l'organisation et le fonctionnement du code est décrit succintement dans une documentation intégrée proprement au dépôt git
  - Une partie "quickstart" (démarrage rapide) est présente dans la documentation pour indiquer les quelques commandes standards pouvant être utilisées pour lancer le code
  - Un diagramme qui identifie les différents modules et acteurs de l'application. 
- De la portabilité
  - Les éventuels paramètres de configuration du projet sont externalisés et surchargeables  
  - Le projet n'a pas d'adhérence à une machine en particulier (liens de fichiers en dur, adhérence à un système d'exploitation)
- De la qualité
  - Au moins un test unitaire est présent
  - La façon de lancer les tests est documentée
- De l'automatisation
  - Bonus : un pipeline s'exécute à chaque push et lance les tests unitaires présents dans le projet
  - Des scripts / de la documentation permettant de démarrer
 
| Critère   |   Barème (/20)    |
|----------|:-------------:|
| Gestion du dépôt git |  2 pts | 
| Documentation (présentation projet) |  2 pts | 
| Portabilité | 3 pts |
| Configuration | 2 pts |
| Structure du code / architecture |    2 pts   | 
| Applicatif fonctionnel | 4 pts |
| Qualité du code - testing | 3 pts |
| Automatisation | 2 pts |
| Bonus | 4 pts | 

### Conditions de l'examen

Les conditions de cet examen se veulent proches de vos futures conditions de travail professionnelles.  
Ainsi, vous avez accès à toutes les ressources que vous souhaitez (internet, stackoverflow, forums, chats). De même, la réutilisation de code est permise, tant que vous en respectez la license d'utilisation.  

Vous êtes libres du language utilisé, il peut aussi bien choisir de python, javascript, java ou autres.

> NB: les mêmes critères s'appliquent sur ces autres languages

Vous êtes aussi libres de choisir votre environnement de travail. Il vous est ainsi par exemple toujours possible d'accéder aux services en ligne de la plateforme SSPCloud : [Datalab](https://datalab.sspcloud.fr) mais vous pouvez tout à fait travailler sur votre environnement local ou sur tout autre environnement qui vous conviendrait.
Enfin, le professeur reste mobilisable, dans la limite du raisonnable, pendant toute la période du projet. Evidemment, comme pour toute demande de débug, il est demandé de joindre le code correspondant (lien vers le dépôt git à jour), le résultat attendu et l'erreur rencontrée (message d'erreur + `stacktrace`).

### Rappel: Validation

Pour la validation de votre code, le dépot git mis en place au cours de ce tp sera cloné, les modalités d'installation seront suivies, l'application sera lancée. 

## Sujet 1 : Application de black jack

Le principe de ce projet repose sur l'utilisation de l'api documentée ici:
https://deckofcardsapi.com/
Cette API met a disposition des decks de 52 de cartes classiques. Elle propose également de nombreuses opérations détaillées dans la documentation.

Cela permet de ne pas avoir a redévelopper toute la logique de tirage de cartes et de distribution.

> Pour pimenter votre application vous pouvez bien évidemment utiliser plusieurs paquets, les mélanger les remettre a zéro comme vous le souhaitez
## Concept

Bobby est un grand joueur, il aime les jeux de cartes. Il désirerait que vous développiez un jeu basé sur le blackjack.

Il permettrait de créer des **joueurs**, qui ont un *nom* et un *solde* (dans une devise quelconque). 

> Les joueurs seraient sauvegardés dans une base de données

Ces **joueurs** pourraient participer a des parties contre des **croupiers**. 

Pour ce qui est du gameplay, il désire pouvoir prendre le contrôle d'un joueur, et jouer une partie de blackjack avec un croupier. 

> NB: vous n'êtes pas nécessairement pas invités à réaliser une implémentation exhaustive des règles, une version simpliste du jeu sera acceptée 

## Sujet 2 : Webservice d'envoi de mail 

Ce sujet est un sujet plus technique portée sur l'utilisation d'API avec jeton d'authentification et/ou serveur SMTP.

Vous pouvez donc aussi bien proposer l'utilisation d'un serveur SMTP local, que d'un serveur SMTP comme celui de google. (avec potentiellement configuration externe)

Vous pouvez également utiliser un service tiers d'envoi de mail et simplement gérer cela par appel API : https://rapidapi.com/blog/email-apis-which-one-is-right-for-you/

## Concept
Bobby est très organisé et pense beaucoup a son travail. Pour ne pas gâcher ses soirées et ses nuits, il souhaiterait avoir accès a un service d'envoi de mail asynchrone.

Par exemple, il souhaite pouvoir envoyer ses mails le soir, avec une heure d'envoi fixée au lendemain matin. Le serveur qui reçoit les demandes, vérifierait les demandes en cours dont la date est dépassée, et le cas échéant enverrait un mail au destinataire de Bobby.

Il faudra donc persister les demandes de bobby de la forme : 

```json
{
  "destinataire":"antoine.brunetti@insee.fr",
  "from":"bobby@eleve.ensai.fr"
  "Subject":"Rendu conception logicielle "
  "contenu":"Voici le lien de notre dépôt git sur gitlab https://gitlab.com/johndoe/conception , on a rendu en retard mais avec notre application, c'est cool non?"
  "date-envoi":"Fri Mar 24 23:41:00 2023"
}
```

> Pour les formats, rien n'est contractuel tant que cela fonctionne
### Attendus côté API

- Pouvoir créer une demande d'envoi par requête `HTTP` `POST` sur `/demande/`, renvoyant la demande avec un id de renseigné
- Pouvoir récupérer une demande par requête `GET` sur  sur `/demande/{demande-id}`, renvoyant la demande et son état en cours
- Pouvoir lister les demandes par requête `GET` sur `/demandes/` , renvoyant toutes les demandes.
## Sujet 3 : Site d'information sur la météo du jour

[![img](https://thumbs.dreamstime.com/b/sunny-face-very-happy-yellow-ray-sunshine-45749786.jpg)]()

Ce sujet est un sujet d'exploration sur la création de pages web dynamiques.

### Concept

Bobby est quelqu'un de plutôt feignant. Lorsqu'il désire sortir, il préfère regarder la météo sur son téléphone plutôt que de regarder dehors. 

Mais cela ne va pas assez vite pour lui, il lui faudrait une application web qui lui renvoie des informations simples en fonction du temps qu'il fait.

L'api https://open-meteo.com/ permet d'obtenir beaucoup d'informations météorologiques dans différents lieux.

L'objectif est donc de créer un site web qui en fonction de la météo générale du jour renverra une animation différente : en fonction de la `Weather interpretation` et de la `Temperature`. 

Bobby se connectera a l'application, et pourra saisir son adresse.

> Bonus: Vous pouvez aussi regarder du côté de la géolocalisation de la requête de bobby.
